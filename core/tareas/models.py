from tabnanny import verbose
from django.db import models
from datetime import datetime 
from core.empleados.models import Empleado

# Create your models here.

class Tarea (models.Model):
    nombre = models.CharField(max_length=100, verbose_name='Nombre')
    descripcion = models.CharField(max_length=250, verbose_name='Descripción')
    fecha_limite = models.DateField(default=datetime.now, verbose_name='Fecha Límite')
    terminada = models.BooleanField(default=False, verbose_name='Terminada')
    # relación con el modelo Empleado. Una tarea -> un empleado. No se permiten borrar empleados con tareas asignadas
    empleado = models.ForeignKey(Empleado, on_delete=models.PROTECT)

    def __str__(self):
        return self.nombre + " " + self.descripcion + " - A cargo de: " + self.empleado.apellidos + " " + self.empleado.nombres

    class Meta:
        verbose_name = 'Tarea'
        verbose_name_plural = 'Tareas'
        db_table = 'tareas'
        ordering = ['fecha_limite']



