from django.urls import path
from core.tareas.views import *

urlpatterns = [
    path('', TareaListView.as_view(), name='tareas'),
    path('crear/', TareaCreateView.as_view(), name='tareas_crear'),
    path('editar/<int:pk>/', TareaUpdateView.as_view(), name='tareas_editar'),
    path('eliminar/<int:pk>/', TareaDeleteView.as_view(), name='tareas_eliminar'),
]