from django.contrib import admin
from core.tareas.models import *
from core.empleados.models import *

# Register your models here.

admin.site.register(Empleado)
admin.site.register(Tarea)

