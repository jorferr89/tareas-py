from .models import *
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .forms import *
from django.urls import reverse_lazy

# Create your views here.

class TareaListView(ListView):
    model = Tarea
    template_name = 'tareas/index.html'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Tareas'
        return context

class TareaCreateView(CreateView):
    model = Tarea
    form_class = TareaForm
    template_name = 'tareas/formulario.html'
    success_url = reverse_lazy('tareas')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Nueva Tarea'
        return context

class TareaUpdateView(UpdateView):
    model = Tarea
    form_class = TareaForm
    template_name = 'tareas/formulario.html'
    success_url = reverse_lazy('tareas')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Editar Tarea'
        return context

class TareaDeleteView(DeleteView):
    model = Tarea
    template_name = 'tareas/eliminar.html'
    success_url = reverse_lazy('tareas')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminar Tarea'
        return context