from django.urls import path
from core.empleados.views import EmpleadoListView, EmpleadoCreateView, EmpleadoUpdateView, EmpleadoDeleteView


urlpatterns = [
    path('', EmpleadoListView.as_view(), name='empleados'),
    path('crear/', EmpleadoCreateView.as_view(), name='empleados_crear'),
    path('editar/<int:pk>/', EmpleadoUpdateView.as_view(), name='empleados_editar'),
    path('eliminar/<int:pk>/', EmpleadoDeleteView.as_view(), name='empleados_eliminar')
]