from django.db import models

# Create your models here.

class Empleado (models.Model):
    codigo_empleado = models.BigIntegerField(default=0, verbose_name='Código')
    apellidos = models.CharField(max_length=100, verbose_name='Apellidos')
    nombres = models.CharField(max_length=100, verbose_name='Nombres')
    sueldo = models.DecimalField(default=0.00, max_digits=6, decimal_places=2)

    def __str__(self):
        return self.apellidos + " " + self.nombres

    class Meta:
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'
        db_table = 'empleados'
        ordering = ['apellidos', 'nombres']
