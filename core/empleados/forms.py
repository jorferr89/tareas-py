from cProfile import label
from tkinter import Widget
from tokenize import Number
from django.forms import *
from .models import *

class EmpleadoForm(ModelForm):
    class Meta:
        model = Empleado
        fields = '__all__'

        labels = {
            'codigo_empleado': 'Código',
            'apellidos': 'Apellidos',
            'nombres': 'Nombres',
            'sueldo': 'Sueldo'
        }
        widgets = {
            'codigo_empleado': NumberInput(
                attrs={
                    'class': 'form-control'
                }
            ),

            'apellidos': TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),

            'nombres': TextInput(
                attrs={
                    'class': 'form-control'
                }
            ),

            'sueldo': NumberInput(
                attrs={
                    'class': 'form-control'
                }
            )

        }